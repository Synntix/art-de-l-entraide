DROP TRIGGER IF EXISTS T_ANNONCE on Annonce;
DROP TRIGGER IF EXISTS T_MESSAGE on Message;
DROP FUNCTION IF EXISTS f_insert_annonce;
DROP FUNCTION IF EXISTS f_insert_message;
DROP TABLE IF EXISTS Reponse;
DROP TABLE IF EXISTS Message;
DROP TABLE IF EXISTS Annonce;
DROP TABLE IF EXISTS Categorie;
DROP TABLE IF EXISTS Utilisateur;
DROP TABLE IF EXISTS Certificateur;
