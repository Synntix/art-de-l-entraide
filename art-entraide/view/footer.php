<footer>

<section>

  <div class="hautdepage">

    <a href="#"><img src="/view/design/Up-Arrow.svg" alt="" title="Retour en haut de page">Haut de page</a>

  </div>

  <div class="mainfooter">

    <div>
      <h3>À propos</h3>
      <div class="text">
        <a href="/controler/pagesinfos.ctrl.php?page=description" title="À propos de L'Art de l'Entraide">Qui sommes-nous ?</a>
        <a href="/controler/pagesinfos.ctrl.php?page=contact" title="Envoyer un message aux créateurs du site">Nous contacter</a>
        <a href="/controler/pagesinfos.ctrl.php?page=FAQ" title="Questions fréquemment posées">F.A.Q.</a>
      </div>
    </div>

    <div>
      <h3>Informations légales</h3>
      <div class="text">
        <a href="/controler/pagesinfos.ctrl.php?page=cgu" title="En utilisant notre site, vous acceptez ces Conditions">Conditions générales d'utilisation</a>
        <a href="/controler/pagesinfos.ctrl.php?page=legal" title="Informations légales sur notre site">Mentions légales</a>
        <a href="/controler/pagesinfos.ctrl.php?page=confidentialite" title="En savoir plus sur l'utilisation de vos données">Politique de confidentialité</a>
      </div>
    </div>

    <div>
      <h3>Suivez nous</h3>
      <div class="reseaux">
        <a class="icon" target="_blank" href="https://twitter.com/" title="Twitter">
          <img alt="Twitter" src="/view/design/Reseaux-sociaux-logos/twitter.png"
           width="35" height="30">
        </a>
        <a class="icon" target="_blank" href="https://www.facebook.com/" title="Facebook">
         <img alt="Facebook" src="/view/design/Reseaux-sociaux-logos/facebook.png"
         width="30" height="30">
        </a>
        <a class="icon" target="_blank" href="https://www.instagram.com/" title="Instagram">
         <img alt="Instagram" src="/view/design/Reseaux-sociaux-logos/instagram.png"
         width="30" height="30">
        </a>
      </div>
    </div>

  </div>

</section>

</footer>
