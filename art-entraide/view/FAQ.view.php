<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content=" initial-scale=1, width=device-width "/>
    <title>FAQ</title>
    <link rel="stylesheet" href="/view/css/master.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;1,400;1,600;1,700&display=swap" rel="stylesheet">
  </head>


  <body>

    <?php include_once(__DIR__."/header.php"); ?>

    <section>
      <h1>Foire Aux Questions</h1>
      <article>
        <h2></h2>
        <p>
          Un problème ? Nous sommes là pour y répondre !<br>
          La réponse à votre question est peut-être déjà ci-dessous.<br>
          Sinon, n'hésitez pas à nous contacter via la page "Nous contacter" en pied de page.
        </p>
      </article>

      <article>
        <h2>Questions récurrentes :</h2>
        <div>
          <dt>Q : Comment créer une annonce ?<dt>
          <dd>R : Il faut dans un premier temps se connecter et pour cela avoir un compte actif. Si ce n'est pas le cas,
            il faudra en créer un via le bouton en haut à droite. Ensuite, cliquer sur "Créer une annonce" et
            compléter les champs.</dd>
          <br>

          <dt>Q : Comment se déconnecter ?</dt>
          <dd>R : Pour vous déconnecter, rendez vous sur votre profil (bouton en haut à droite) puis cliquez sur "Se déconnecter"</dd>
        </div>
      </article>
    </section>

    <?php include_once(__DIR__."/footer.php"); ?>
  </body>
</html>
