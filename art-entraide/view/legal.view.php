<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content=" initial-scale=1, width=device-width "/>
    <title>Mentions légales</title>
    <link rel="stylesheet" href="/view/css/master.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;1,400;1,600;1,700&display=swap" rel="stylesheet">
  </head>


  <body>

    <?php include_once(__DIR__."/header.php"); ?>



    <section>

      <h1>Mentions légales</h1>
      <p>
        L'art de l'entraide <br>
        2 Place Doyen Gosse <br>
        38000 Grenoble <br>
        France <br><br>

        vealem@etu.univ-grenoble-alpes.fr <br>

        Ce site internet est hébergé sur un serveur privé appartenant à l'un des fondateurs.<br>
        
      </p>

    </section>

    <?php include_once(__DIR__."/footer.php"); ?>
  </body>
</html>
