<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content=" initial-scale=1, width=device-width "/>
    <title>Politique de confidentialité</title>
    <link rel="stylesheet" href="/view/css/master.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;1,400;1,600;1,700&display=swap" rel="stylesheet">
  </head>


  <body>

    <?php include_once(__DIR__."/header.php"); ?>



    <section>

      <h1>DÉCLARATION DE CONFIDENTIALITÉ</h1>
      <p>

        ----<br><br>

        <h2>ARTICLE 1 – RENSEIGNEMENTS PERSONNELS RECUEILLIS</h2>

        Lorsque vous créez votre compte, nous recueillons les renseignements personnels que vous nous fournissez, tels que votre nom, votre adresse et votre adresse e-mail.<br>

        Lorsque vous naviguez sur notre site, nous recevons également automatiquement l’adresse de protocole Internet (adresse IP) de votre ordinateur, qui nous permet d’obtenir plus de détails au sujet du navigateur et du système d’exploitation que vous utilisez.<br>


        <h2>ARTICLE 2 - CONSENTEMENT</h2>

        Comment obtenez-vous mon consentement?<br>

        Lorsque vous nous fournissez vos renseignements personnels pour la création de votre compte, nous présumons que vous consentez à ce que nous recueillions vos renseignements et à ce que nous les utilisions à cette fin uniquement.<br>


        Comment puis-je retirer mon consentement?<br>

        Si après nous avoir donné votre consentement, vous changez d’avis et ne consentez plus à ce que nous puissions vous contacter, recueillir vos renseignements ou les divulguer, vous pouvez nous en aviser en nous contactant à vealem@etu.univ-grenoble-alpes.fr, par courrier à: 2 Place Doyen Gosse, Grenoble, 38000, France ou par la rubrique "Nous Contacter" de notre site.<br>

        De plus, en cas de suppression de votre compte, toutes les informations et données liées à votre personne seront automatiquement supprimées de notre base de donnée.<br>


        <h2>ARTICLE 3 – DIVULGATION</h2>

        Nous pouvons divulguer vos renseignements personnels si la loi nous oblige à le faire ou si vous violez nos Conditions Générales d’Utilisation.<br>


        <h2>ARTICLE 4 – ART-ENTRAIDE</h2>

        Notre site internet est hébergé sur un serveur privé de l'un de nos fondateurs.<br>

        Vos données sont stockées dans le système de stockage de données et les bases de données de notre serveur privé. Vos données sont conservées sur un serveur sécurisé protégé par un pare-feu.<br>


        <h2>ARTICLE 5 – SERVICES FOURNIS PAR DES TIERS</h2>

        Une fois que vous quittez le site ou que vous êtes redirigé vers le site web ou l’application d’un tiers, vous n’êtes plus régi par la présente Politique de Confidentialité ni par les Conditions Générales de Vente et d’Utilisation de notre site web.<br>


        <h3>Liens<h3>

        Vous pourriez être amené à quitter notre site web en cliquant sur certains liens présents sur notre site. Nous n’assumons aucune responsabilité quant aux pratiques de confidentialité exercées par ces autres sites et vous recommandons de lire attentivement leurs politiques de confidentialité.<br>


        <h2>ARTICLE 6 – SÉCURITÉ</h2>

        Pour protéger vos données personnelles, nous prenons des précautions raisonnables et suivons les meilleures pratiques de l’industrie pour nous assurer qu’elles ne soient pas perdues, détournées, consultées, divulguées, modifiées ou détruites de manière inappropriée.<br>


        <h2>FICHIERS TÉMOINS (COOKIES)</h2>

        Voici une liste de fichiers témoins que nous utilisons. Nous les avons énumérés ici pour que vous ayez la possibilité de choisir si vous souhaitez les autoriser ou non.<br>

        _session_id, identificateur unique de session, permet à Shopify de stocker les informations relatives à votre session (référent, page de renvoi, etc.).<br>


        <h2>ARTICLE 7 – ÂGE DE CONSENTEMENT</h2>

        En utilisant ce site, vous déclarez que vous avez au moins l’âge de la majorité dans votre État ou province de résidence, et que vous nous avez donné votre consentement pour permettre à toute personne d’âge mineur à votre charge d’utiliser ce site web.<br>


        <h2>ARTICLE 8 – MODIFICATIONS APPORTÉES À LA PRÉSENTE POLITIQUE DE CONFIDENTIALITÉ</h2>

        Nous nous réservons le droit de modifier la présente politique de confidentialité à tout moment, donc veuillez s’il vous plait la consulter fréquemment. Les changements et les clarifications prendront effet immédiatement après leur publication sur le site web. Si nous apportons des changements au contenu de cette politique, nous vous aviserons ici qu’elle a été mise à jour, pour que vous sachiez quels renseignements nous recueillons, la manière dont nous les utilisons, et dans quelles circonstances nous les divulguons, s’il y a lieu de le faire.<br>


        <h2>QUESTIONS ET COORDONNÉES</h2>

        Si vous souhaitez: accéder à, corriger, modifier ou supprimer toute information personnelle que nous avons à votre sujet, déposer une plainte, ou si vous souhaitez simplement avoir plus d’informations, contactez notre agent responsable des normes de confidentialité à vealem@etu.univ-grenoble-alpes.fr ou par courrier à l'adresse ci-dessous:v<br>

        [2 Place Doyen Gosse, Grenoble, 38000, France]<br>

        ----
      </p>

    </section>

    <?php include_once(__DIR__."/footer.php"); ?>
  </body>
</html>
