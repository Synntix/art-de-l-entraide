<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content=" initial-scale=1, width=device-width "/>
    <title>Condition général d'utilisation</title>
    <link rel="stylesheet" href="/view/css/master.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;1,400;1,600;1,700&display=swap" rel="stylesheet">
  </head>


  <body>

    <?php include_once(__DIR__."/header.php"); ?>



    <section>
      <h1>CONDITIONS GÉNÉRALES D’UTILISATION</h1>

      ----<br><br>

      <h2>VUE D’ENSEMBLE</h2>

      Ce site web est exploité par L'art de l'entraide. Sur ce site, les termes "nous", "notre" et "nos" font référence à L'art de l'entraide. L'art de l'entraide propose ce site web, y compris toutes les informations, tous les outils et tous les services qui y sont disponibles pour vous, l’utilisateur, sous réserve de votre acceptation de l’ensemble des modalités, conditions, politiques et avis énoncés ici.
      <br>
      En visitant ce site, vous vous engagez dans notre "Service" et acceptez d’être lié par les modalités suivantes ("Conditions Générales d’Utilisation", "Conditions"), y compris les modalités, conditions et politiques additionnelles auxquelles il est fait référence ici et/ou accessibles par hyperlien. Ces Conditions Générales d’Utilisation s’appliquent à tous les utilisateurs de ce site, incluant mais ne se limitant pas, aux utilisateurs qui naviguent sur le site et aux contributeurs de contenu.
      <br>
      Veuillez lire attentivement ces Conditions Générales de Vente et d’Utilisation avant d’accéder à ou d’utiliser notre site web. En accédant à ou en utilisant une quelconque partie de ce site, vous acceptez d’être lié par ces Conditions Générales d’Utilisation. Si vous n’acceptez pas toutes les modalités et toutes les Conditions de cet accord, alors vous ne devez pas accéder au site web ni utiliser les services qui y sont proposés. Si ces Conditions Générales d’Utilisation sont considérées comme une offre, l’acceptation se limite expressément à ces Conditions Générales de Vente et d’Utilisation.
      <br>
      Toutes les nouvelles fonctionnalités et tous les nouveaux outils qui seront ajoutés ultérieurement à cette boutique seront également assujettis à ces Conditions Générales d’Utilisation. Vous pouvez consulter la version la plus récente des Conditions Générales de d’Utilisation à tout moment sur cette page. Nous nous réservons le droit de mettre à jour, de changer ou de remplacer toute partie de ces Conditions Générales d’Utilisation en publiant les mises à jour et/ou les modifications sur notre site web. Il vous incombe de consulter cette page régulièrement pour vérifier si des modifications ont été apportées. Votre utilisation continue du site web ou votre accès à celui-ci après la publication de toute modification constitue une acception de votre part de ces modifications.
      <br>
      Notre boutique est hébergée sur un serveur privé appartenant à l'un des fondateurs.
      <br>
      <h2>ARTICLE 1 – CONDITIONS D’UTILISATION DE NOTRE BOUTIQUE EN LIGNE</h2>

      En acceptant ces Conditions Générales d’Utilisation, vous déclarez que vous avez atteint l’âge de la majorité dans votre pays, État ou province de résidence, et que vous nous avez donné votre consentement pour permettre à toute personne d’âge mineur à votre charge d’utiliser ce site web.
      <br>
      L’utilisation de nos produits à toute fin illégale ou non autorisée est interdite, et vous ne devez pas non plus, dans le cadre de l’utilisation du Service, violer les lois de votre juridiction (incluant mais ne se limitant pas aux lois relatives aux droits d’auteur).
      <br>
      Vous ne devez pas transmettre de vers, de virus ou tout autre code de nature destructive.
      <br>
      Toute infraction ou violation des présentes Conditions Générales de d’Utilisation entraînera la résiliation immédiate de vos Services.
      <br>

      <h2>ARTICLE 2 – CONDITIONS GÉNÉRALES</h2>

      Nous nous réservons le droit de refuser à tout moment l’accès aux services à toute personne, et ce, pour quelque raison que ce soit.
      <br>
      Vous acceptez de ne pas reproduire, dupliquer, copier, vendre, revendre ou exploiter une quelconque partie du Service ou utilisation du Service, ou un quelconque accès au Service ou contact sur le site web, par le biais duquel le Service est fourni, sans autorisation écrite expresse préalable de notre part.
      <br>
      Les titres utilisés dans cet accord sont inclus pour votre commodité, et ne vont ni limiter ni affecter ces Conditions.
      <br>

      <h2>ARTICLE 3 – EXACTITUDE, EXHAUSTIVITÉ ET ACTUALITÉ DES INFORMATIONS</h2>

      Nous ne sommes pas responsables si les informations disponibles sur ce site ne sont pas exactes, complètes ou à jour. Le contenu de ce site est fourni à titre indicatif uniquement et ne devrait pas constituer votre seule source d’information pour prendre des décisions, sans consulter au préalable des sources d’information plus exactes, plus complètes et actualisées. Si vous décidez de vous fier au contenu présenté sur ce site, vous le faites à votre propre risque.
      <br>
      Ce site pourrait contenir certaines informations antérieures. Ces informations antérieures, par nature, ne sont pas à jour et sont fournies à titre indicatif seulement. Nous nous réservons le droit de modifier le contenu de ce site à tout moment, mais nous n’avons aucune obligation de mettre à jour les informations sur notre site. Vous acceptez qu’il vous incombe de surveiller les modifications apportées à notre site.
      <br>

      <h2>ARTICLE 4 – MODIFICATIONS APPORTÉES AU SERVICE ET AUX PRIX</h2>

      Nous nous réservons le droit à tout moment de modifier ou d’interrompre le Service (ainsi que toute partie ou tout contenu du Service) sans préavis et en tout temps.
      <br>
      Nous ne serons pas responsables envers vous ou toute autre tierce partie de toute suspension ou interruption du Service.
      <br>

      <h2>ARTICLE 5 – EXACTITUDE DES INFORMATIONS DE COMPTE</h2>

      Vous acceptez de fournir des informations de compte à jour, complètes et exactes. Vous vous engagez à mettre à jour rapidement votre compte et vos autres informations, y compris votre adresse e-mail, pour que nous puissions vous contacter si nécessaire.
      <br>

      <h2>ARTICLE 6 – LIENS DE TIERS</h2>

      Certains contenus, produits et services disponibles par le biais de notre Service pourraient inclure des éléments provenant de tierces parties.
      <br>
      Les liens provenant de tierces parties sur ce site pourraient vous rediriger vers des sites web de tiers qui ne sont pas affiliés à nous. Nous ne sommes pas tenus d’examiner ou d’évaluer le contenu ou l’exactitude de ces sites, et nous ne garantissons pas et n’assumons aucune responsabilité quant à tout contenu, site web, produit, service ou autre élément accessible sur ou depuis ces sites tiers.
      <br>
      Nous ne sommes pas responsables des préjudices ou dommages liés à l’achat ou à l’utilisation de biens, de services, de ressources, de contenu, ou de toute autre transaction effectuée en rapport avec ces sites web de tiers. Veuillez lire attentivement les politiques et pratiques des tierces parties et assurez-vous de bien les comprendre avant de vous engager dans toute transaction. Les plaintes, réclamations, préoccupations, ou questions concernant les produits de ces tiers doivent être soumises à ces mêmes tiers.
      <br>

      <h2>ARTICLE 7 – RENSEIGNEMENTS PERSONNELS</h2>

      La soumission de vos renseignements personnels sur notre site est régie par notre Politique de Confidentialité. Notre Politique de Confidentialité est accessible dans le menu pied de page.
      <br>

      <h2>ARTICLE 8 – UTILISATIONS INTERDITES</h2>

      En plus des interdictions énoncées dans les Conditions Générales de Vente et d’Utilisation, il vous est interdit d’utiliser le site ou son contenu: (a) à des fins illégales; (b) pour inciter des tiers à réaliser des actes illégaux ou à y prendre part; (c) pour enfreindre toute ordonnance régionale ou toute loi, règle ou régulation internationale, fédérale, provinciale ou étatique; (d) pour porter atteinte à ou violer nos droits de propriété intellectuelle ou ceux de tierces parties; (e) pour harceler, maltraiter, insulter, blesser, diffamer, calomnier, dénigrer, intimider ou discriminer quiconque en fonction du sexe, de l’orientation sexuelle, de la religion, de l’origine ethnique, de la race, de l’âge, de l’origine nationale, ou d’un handicap; (f) pour soumettre des renseignements faux ou trompeurs; (g) pour téléverser ou transmettre des virus ou tout autre type de code malveillant qui sera ou pourrait être utilisé de manière à compromettre la fonctionnalité ou le fonctionnement du Service ou de tout autre site web associé, indépendant, ou d’Internet; (h) pour recueillir ou suivre les renseignements personnels d’autrui; (i) pour polluposter, hameçonner, détourner un domaine, extorquer des informations, parcourir, explorer ou balayer le web (ou toute autre ressource); (j) à des fins obscènes ou immorales; ou (k) pour porter atteinte ou contourner les mesures de sécurité de notre Service, de tout autre site web, ou d’Internet. Nous nous réservons le droit de résilier votre utilisation du Service ou de tout site web connexe pour avoir enfreint les utilisations interdites.
      <br>

      <h2>ARTICLE 9 – EXCLUSION DE GARANTIES ET LIMITATION DE RESPONSABILITÉ</h2>

      Nous ne garantissons ni ne prétendons en aucun cas que votre utilisation de notre Service sera ininterrompue, rapide, sécurisée ou sans erreur.
      <br>
      Nous ne garantissons pas que les résultats qui pourraient être obtenus par le biais de l’utilisation du Service seront exacts ou fiables.
      <br>
      Vous acceptez que de temps à autre, nous puissions supprimer le Service pour des périodes de temps indéfinies ou annuler le Service à tout moment, sans vous avertir au préalable.
      <br>
      Vous convenez expressément que votre utilisation du Service, ou votre incapacité à utiliser celui-ci, est à votre seul risque. Le Service ainsi que tous les services qui vous sont fournis par le biais du Service sont (sauf mention expresse du contraire de notre part) fournis "tels quels" et "selon la disponibilité" pour votre utilisation, et ce sans représentation, sans garanties et sans conditions d'aucune sorte, expresses ou implicites.
      <br>
      L'art de l'entraide ne peut en aucun cas être tenu responsable de toute blessure, perte, réclamation, ou de dommages directs, indirects, accessoires, punitifs, spéciaux, ou dommages consécutifs de quelque nature qu’ils soient, incluant mais ne se limitant pas à la perte de profits, de revenus, d’économies, de données, aux coûts de remplacement ou tous dommages similaires, qu’ils soient contractuels, délictuels (même en cas de négligence), de responsabilité stricte ou autre, résultant de votre utilisation de tout service ou produit provenant de ce Service, ou quant à toute autre réclamation liée de quelque manière que ce soit à votre utilisation du Service ou de tout produit, incluant mais ne se limitant à toute erreur ou omission dans tout contenu, ou à toute perte ou tout dommage de toute sorte découlant de l’utilisation du Service ou de tout contenu (ou produit) publié, transmis, ou autrement rendu disponible par le biais du Service, même si vous avez été avertis de la possibilité qu’ils surviennent. Parce que certains États ou certaines juridictions ne permettent pas d’exclure ou de limiter la responsabilité quant aux dommages consécutifs ou accessoires, notre responsabilité sera limitée dans la mesure maximale permise par la loi.
      <br>


      <h2>ARTICLE 10 – INDEMNISATION</h2>

      Vous acceptez d’indemniser, de défendre et de protéger L'art de l'entraide, quant à toute réclamation ou demande, incluant les honoraires raisonnables d’avocat, faite par toute tierce partie à cause de ou découlant de votre violation de ces Conditions Générales de Vente et d’Utilisation ou des documents auxquels ils font référence, ou de votre violation de toute loi ou des droits d’un tiers.
      <br>

      <h2>ARTICLE 11 – DISSOCIABILITÉ</h2>

      Dans le cas où une disposition des présentes Conditions Générales d’Utilisation serait jugée comme étant illégale, nulle ou inapplicable, cette disposition pourra néanmoins être appliquée dans la pleine mesure permise par la loi, et la partie non applicable devra être considérée comme étant dissociée de ces Conditions Générales de Vente et d’Utilisation, cette dissociation ne devra pas affecter la validité et l’applicabilité de toutes les autres dispositions restantes.
      <br>

      <h2>ARTICLE 12 – RÉSILIATION</h2>

      Les obligations et responsabilités engagées par les parties avant la date de résiliation resteront en vigueur après la résiliation de cet accord et ce à toutes les fins.
      <br>
      Ces Conditions Générales d’Utilisation sont effectives à moins et jusqu’à ce qu’elles soient résiliées par ou bien vous ou non. Vous pouvez résilier ces Conditions Générales de Vente et d’Utilisation à tout moment en nous avisant que vous ne souhaitez plus utiliser nos Services, ou lorsque vous cessez d’utiliser notre site.
      <br>
      Si nous jugeons, à notre seule discrétion, que vous échouez, ou si nous soupçonnons que vous avez été incapable de vous conformer aux modalités de ces Conditions Générales d’Utilisation, nous pourrions aussi résilier cet accord à tout moment sans vous prévenir à l’avance et/ou nous pourrions vous refuser l’accès à nos Services (ou à toute partie de ceux-ci).
      <br>

      <h2>ARTICLE 13 – INTÉGRALITÉ DE L’ACCORD</h2>

      Tout manquement de notre part à l’exercice ou à l’application de tout droit ou de toute disposition des présentes Conditions Générales de Vente et d’Utilisation ne devrait pas constituer une renonciation à ce droit ou à cette disposition.
      <br>
      Ces Conditions Générales d’Utilisation ou toute autre politique ou règle d’exploitation que nous publions sur ce site ou relativement au Service constituent l’intégralité de l’entente et de l’accord entre vous et nous et régissent votre utilisation du Service, et remplacent toutes les communications, propositions et tous les accords, antérieurs et contemporains, oraux ou écrits, entre vous et nous (incluant, mais ne se limitant pas à toute version antérieure des Conditions Générales d’Utilisation).
      <br>
      Toute ambiguïté quant à l’interprétation de ces Conditions Générales d’Utilisation ne doit pas être interprétée en défaveur de la partie rédactrice.
      <br>

      <h2>ARTICLE 14 – LOI APPLICABLE</h2>

      Ces Conditions Générales d’Utilisation, ainsi que tout autre accord séparé par le biais duquel nous vous fournissons des Services seront régis et interprétés en vertu des lois en vigueur à 2 Place Doyen Gosse, Grenoble, 38000, France.
      <br>

      <h2>ARTICLE 15 – MODIFICATIONS APPORTÉES AUX CONDITIONS GÉNÉRALES D’UTILISATION</h2>

      Vous pouvez consulter la version la plus récente des Conditions Générales d’Utilisation à tout moment sur cette page.
      <br>
      Nous nous réservons le droit, à notre seule discrétion, de mettre à jour, de modifier ou de remplacer toute partie de ces Conditions Générales d’Utilisation en publiant les mises à jour et les changements sur notre site. Il vous incombe de visiter notre site régulièrement pour vérifier si des changements ont été apportés. Votre utilisation continue de ou votre accès à notre site après la publication de toute modification apportée à ces Conditions Générales d’Utilisation constitue une acceptation de ces modifications.
      <br>

      <h2>ARTICLE 16 – COORDONNÉES</h2>

      Les questions concernant les Conditions Générales d’Utilisation devraient nous être envoyées à vealem@etu.univ-grenoble-alpes.fr.
      <br>
      ----
    </section>

    <?php include_once(__DIR__."/footer.php"); ?>
  </body>
</html>
