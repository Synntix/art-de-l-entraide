<?php
// ============ Controleur qui gère une annonce ============ //

// Inclusion du framework
include_once(__DIR__."/../framework/view.class.php");
// Inclusion du modèle
include_once(__DIR__."/../model/DAO.class.php");
include_once(__DIR__."/../model/Categorie.class.php");

// ==== PARTIE RECUPERATION DES DONNEES ==== //
$idAnnonce = htmlentities($_GET['idAnnonce']);

// ==== PARTIE USAGE DU MODELE ==== //
session_start();
$art = new DAO();
//récupération de l'annonce
$annonce = $art->getAnnonce((int)$idAnnonce);

//récupération du nom de l'auteur
$nomAuteur = $annonce->getCreateur()->getPrenom() . ' ' . $annonce->getCreateur()->getNom();

//récupération du nom de la catégorie
$categorie = $annonce->getCategorie();
$nomCategorie = $categorie->getNom();

//recuperation du user si il existe
$user = $_SESSION['user'];
$categories = $_SESSION['nomCategories'];

session_write_close();

// ==== PARTIE SELECTION DE LA VUE ==== //
$view = new View();
//$view->assign('error',$error);
//$view->display("annonce.view.php");

$view->assign('annonce', $annonce);
$view->assign('nomAuteur',$nomAuteur);
$view->assign('nomCategorie',$nomCategorie);
//information nécessaire pour le header
$view->assign('nomCategories', $categories);
$view->assign('user', $user);

$view->display("annonce.view.php");
?>
